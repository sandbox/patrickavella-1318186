<?

function _share_bar_list_badges() {
	return array(
		'Digg' => '
			<script>
				(function() {
				var s = document.createElement("SCRIPT"), s1 = document.getElementsByTagName("SCRIPT")[0];
				s.type = "text/javascript";
				s.async = true;
				s.src = "http://widgets.digg.com/buttons.js";
				s1.parentNode.insertBefore(s, s1);
				})();
			</script>
			<a class="DiggThisButton DiggMedium"></a>
		',
		'Facebook' => '
			<script>
			(function() {
				document.write("<iframe src=\"http://www.facebook.com/plugins/like.php?href="+document.URL+"&amp;layout=box_count&amp;show_faces=true&amp;width=50&amp;action=like&amp;colorscheme=light&amp;height=65\" scrolling=\"no\" frameborder=\"0\" style=\"border:none; overflow:hidden; width:50px; height:65px;\" allowTransparency=\"true\"></iframe>");
			})();
			</script>
		',
		'Reddit' => '<script type="text/javascript" src="http://www.reddit.com/static/button/button2.js"></script>',
		'Twitter' => '
			<script>
			(function() {
				document.write("<a href=\"http://twitter.com/share\" class=\"twitter-share-button\"\
				data-url=\""+document.URL+"\"\
				data-text=\""+document.title+"\"\
				data-count=\"vertical\">Tweet</a>\
				");
			})();
			</script>
		',
		'GooglePlus' => '<g:plusone size="tall"></g:plusone>',
		'TweetMeme' => '<script type="text/javascript" src="http://tweetmeme.com/i/scripts/button.js"></script>',
		'StumbleUpon' => '<script src="http://www.stumbleupon.com/hostedbadge.php?s=5"></script>',
		'LinkedIn' => '
			<script src="http://platform.linkedin.com/in.js" type="text/javascript"></script>
			<script type="IN/Share" data-counter="top"></script>
		',
	);
}
function _share_bar_external_js() {
	return array(
		'Twitter' => 'http://platform.twitter.com/widgets.js',
		'GooglePlus' => 'http://apis.google.com/js/plusone.js'
	);
}