(function ($) {
	$(function(){
		// We only care about the width of share bar
		// when it's actively sticking.
		Drupal.settings.share_bar_fixed_width=0;
		update_badge_position();
	});

	$(window).resize(function() {
		update_badge_position();	
	});

	function update_badge_position(){
		var myleft, // top left corner of the badges
		    myright; //top right corner of badges when in fixed state
		if($("#share-bar-badges").css("position") == "fixed") {
			Drupal.settings.share_bar_fixed_width=$("#share-bar-badges").width();
		}
		
		myleft= $(Drupal.settings.share_bar_sticky).offset().left
		          + parseInt(Drupal.settings.share_bar_leftoffset);
		myright= $(Drupal.settings.share_bar_sticky).offset().left
		          + parseInt(Drupal.settings.share_bar_leftoffset)
		          + Drupal.settings.share_bar_fixed_width;


		if(myleft >= 0 && myright <= $(window).width()) {
			$("#share-bar-badges").addClass("share-bar-badges-fixed").removeClass("share-bar-badges-static");
			$("#share-bar-badges").css({
				"left": ($(Drupal.settings.share_bar_sticky).offset().left + parseInt(Drupal.settings.share_bar_leftoffset))+"px",
				"top": ($(Drupal.settings.share_bar_sticky).offset().top+ parseInt(Drupal.settings.share_bar_topoffset))+"px",
				"position":"fixed"
			});
 		} else {
			$("#share-bar-badges").addClass("share-bar-badges-static").removeClass("share-bar-badges-fixed");
 			$("#share-bar-badges").css({
 				"position":"static"
			});
 		}
	}

})(jQuery);


	
